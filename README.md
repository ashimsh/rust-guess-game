# Number Guessing Game

A command-line implementation of the number guessing game in Rust. The game generates a random number between 1 and 100, and the player has to guess it within a certain number of attempts.

## Installation

```bash
git clone https://gitlab.com/username/rust-guess-game.git
cd rust-guess-game
cargo build --release
```

## Usage

```rust
./target/release/guessing-game 
     OR 
cargo run
```
To start a new game, press "Enter". To make a guess, enter a number between 1 and 100 and press "Enter".

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
